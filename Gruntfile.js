module.exports = function(grunt){
    grunt.initConfig({
        sass:{
            dist:{
                files:[{
                    expand:true,
                    cwd: 'css',
                    src:['*scss'],
                    dest:'css',
                    ext:'.css'
                }]
            }
        },
        watch:{
            files:['css/*.scss'],
            tasks: ['css']
        },
        browserSync :{
            dev:{
               bsfiles:{
                   src: [
                       'css/*.scss',
                       '*.html',
                       'js/*.js'
                   ]
               },
            options: {
                watchTask: true,
                server: {
                    baseDir: './'
                }
            } 
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.registerTask('css',['sass']);
    grunt.registerTask('default',['browserSync','watch']);
};