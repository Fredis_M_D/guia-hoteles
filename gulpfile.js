'use strict'

 
let gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync');

sass.compiler = require('node-sass');

gulp.task('sass', () => {
    gulp.src('./css/*.scss')
        .pipe(sass().on('error',sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', () =>{
    gulp.watch('./css/.*scss',['sass']);
});
gulp.task('browser-sync', () =>{
    var files = ['./*.html','./css/*.css','./img/*.{png, jpg, gif}','.js/*.js']
    browserSync.init( files, {
        server:{
            baseDir: './'
        }
    });
});

gulp.task('default', ['browser-sync'], () =>{
    gulp.start('sass:watch');
})